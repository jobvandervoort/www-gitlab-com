---
layout: markdown_page
title: "Access Requests (AR)"
---
# So you want to get access to a system/group/email alias/vault/etc?

1. Choose a template based on your needs: most people use the Bulk or Single Person templates
1. Do not open an Access Request for anything that is part of a baseline entitlement unless it got missed during onboarding. 
    1. [All team members baseline entitlements](/handbook/business-ops/it-ops-team/#baseline-entitlements)
    1. [Role-based baseline entitlements](/handbook/engineering/security/#baseline-role-based-entitlements-access-runbooks--issue-templates)
1. You must have the label `manager approved` on the issue unless the person is:
    1. an internal team member being added to a g-suite email alias or group
    1. an internal team member being added to a slack group
1. Make sure to assign the issue to the [people who provision access to the system.](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0) 
1. If you need help, please ask IT-Ops in the slack channel #it-ops with a link to the issue you need help with.
1. Only ask for the least amount of access to do the work.
1. You don't need an AR for Zendesk light access. [Follow the instructions to get access by email.]((/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff))

# I need access to version.gitlab.com or license.gitlab.com
[Test if you have a dev account.](https://dev.gitlab.org/)
* If you need a dev account, open an Single Person Access request.
* If you have a dev account, go to [license](https://license.gitlab.com/) and [version](https://version.gitlab.com/users/sign_in) and login with GitLab and authorize them to use your credentials.



## On this page
{:.no_toc}

- TOC
{:toc}

---

## Bulk Access Request Instructions
*Do you have a bunch of people with the same manager to add to the same system/vault/group?* 

> Note: Admin access cannot be granted by bulk, please open single person requests

###### Instructions:

> 1. Title issue: Bulk Access, Name of system/group/vault/etc
> 1. Add all the email addresses or aliases depending on the tool separated by commas.
> 1. Add the label `manager approved` and `ready for provisioning` to the issue.
> 1. Assign the issue to the system provisioner [listed in the tech stack.](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0)
> 1. Close the issue when it's complete.

---

## Single Person Access Request Instructions
*Do you have one person who needs access to a single system or one person who needs access to multiple systems?* 

###### Instructions

> 1. Title issue "Full Name Access Level System" using your information
> 1. Remove or add rows for the systems you need access to in the "ACCOUNT CREATION INFORMATION" and/or "SUB-RESOURCE ACCESS REQUEST" table(s).
> 1. Fill out the tables with the type of access you need
        1. SUB-RESOURCE ACCESS REQUEST: For multiple groups/projects/vaults/channels/vaults
        1. ACCOUNT CREATION ACCESS REQUEST: For new access to a system that is not a baseline entitlement
> 1. Request the least amount of access required to complete your job as per the [least privilege review](/handbook/engineering/security/index.html#least-privilege-reviews-for-access-requests)
> 1. Assign the issue to your manager for approval and leave a comment to your manager to add their approval by adding the label `manager approved` and `ready for provisioning`. 
> 1. Assign this issue to the system's access request provisioner so they can be alerted to issue your credentials. Find a list of the access request provisioners in the [Tech Stack Application documentation](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0). If you can't find an admin from this list or it is unclear, please @ mention `gitlab-com/business-ops/itops`  who will be able to assist, with no particular SLA. If your request is urgent, ping #it-ops in slack with a note on why it is urgent. 
> 1. Add your ssh key in a comment if you need ssh access.
> 1. For requests involving access to critical Infrastructure systems, an additional layer of approval is required by the infrastructure manager team. You must @ mention `Infrastructure-Managers` and ask them to add the label (if approved), `InfrastructureApproved`.
> 1. Close the issue once your requests have been granted or denied.


---


### MANAGER APPROVAL

1. Issues should only be approved after carefully considering whether the requestor needs the permissions outlined. Every review should include a [least privilege review](/handbook/engineering/security/index.html#least-privilege-reviews-for-access-requests)
1. Add your approval by adding the label `manager approved` and `ready for provisioning`.  
1. If you do not approve, add a comment and close the issue.
1. If you are unsure whether the requestor needs the permissions outlined to fulfill their duties, mention `@gitlab-com/gl-security/compliance` in a comment for assistance

---

### ADMINS/SYSTEM PROVISIONERS

1. Carefully review the rationale provided by the requestor to determine whether the access level is necessary or if a lower access level would be sufficient.  Review the [Least Privilege](/handbook/engineering/security/index.html#least-privilege-reviews-for-access-requests) write-up for guidance.
1. If the access level is adequate proceed with provisioning the account after verifying the `manager approved` label is present.
1. Edit the last column in the table to `yes` or `no` once you issue credentials/access or not, so it's clear you responded to the request.
1. If administrative access is being granted, mention `@gitlab-com/gl-security/secops` in a comment and add the label `admin-access` to this request so Security Operations knows who has admin access.
1. If requesting admin access, the system admin should additionally add the labels `SystemOwnerApproved` and `AdminLevelAccess` labels 

---




