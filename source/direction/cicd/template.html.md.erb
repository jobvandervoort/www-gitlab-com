---
layout: markdown_page
title: "Product Vision - CI/CD"
---

- TOC
{:toc}

## Overview

Our vision for CI/CD is aligned to the one set forth in Jez Humble's classic [Continuous Delivery](https://continuousdelivery.com/) book.
The ideas of reducing risk, getting features and fixes out quickly, and reducing costs/creating happier teams by removing the barriers
to getting things done has stood the test of time. 

The CI/CD section focuses on the code build/verification ([Verify](/direction/verify)), packaging/distribution
([Package](/direction/package)), and delivery ([Release](/direction/release)) stages of the
[DevOps Lifecycle](/stages-devops-lifecycle/). Each of these areas has their own strategy
page with upcoming features, north star directions, and more. This page ties them together via important concepts
that unify the direction across all of these areas. In addition to the core CI/CD elements, the 
[mobile use case](/direction/mobile) is very much related and will be of interest to you if you are interested
in building and releasing mobile applications using GitLab.

If you'd like to discuss this vision directly with the product director for CI/CD, 
feel free to reach out to Jason Lenny via [e-mail](mailto:jason@gitlab.com) or on [Twitter](https://twitter.com/j4lenn).
Your contribution is more than welcome.

You can see how the CI/CD stages relate to each other in the following infographic:

![Pipeline Infographic](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_extended_11_11.png "Pipeline Infographic")

## Theme: Multi-Platform Support

GitLab has traditionally done very well with building and deploying software on Linux, with our shared runner fleet
making it very easy to get up and running quickly. We've supported MacOS and Windows Runners, but only in a bring-your-own
configuration where you host and register your own Runners. We're looking to change that by improving Runner features
for these platforms in addition to providing them as available options in the shared runner fleet.

Just being able to run builds on the platform isn't sufficient, though. We also want to improve our support for unit testing
and code coverage frameworks used on these operating systems, as well as our support for different package managers that
are used by code build there.

The action for improving multi-platform support in GitLab is primarily happening in the [Continuous Integration](/direction/verify/continuous_integration)
category, where we're adding this improved testing and runner support, but all categories are involved to some extent
to making sure that our Mac and Windows support is just as good as it is for Linux. If you're a user of these platforms
and have ideas how we can make our support better for your use cases, we'd love to hear them.

## Theme: Progressive Delivery

Put simply, Progressive Delivery is a set of emerging CD best practices, oriented around being able to control
and monitor deployments incrementally over time, and in an automated and safe way. Similarly to Compliance as
Code, our single application is uniquely capable of providing an integrated solution in this space.

Additional articles on Progressive Delivery can be found on the [LaunchDarkly blog](https://launchdarkly.com/blog/progressive-delivery-a-history-condensed/),
[RedMonk](https://redmonk.com/jgovernor/2018/08/06/towards-progressive-delivery/) and 
[The New Stack](https://thenewstack.io/the-rise-of-progressive-delivery-for-systems-resilience/), as well as
our own blog posts highlighting [Feature Flags](/2019/08/06/feature-flags-continuous-delivery/)
and [Review Apps](/2019/04/19/progressive-delivery-using-review-apps/). 

[Continuous Delivery](/direction/release/continuous_delivery/) is the base category for
Progressive Delivery, but there are additional ones that will be important in rounding out our comprehensive solution:

- [Feature Flags](/direction/release/feature_flags/) for targeted rollouts
- [Review Apps](/direction/release/review_apps/) for on-demand validation environments
- [Tracing](https://gitlab.com/groups/gitlab-org/-/epics/89) for behavior analysis across progressive deployments

## Theme: Compliance as Code

Compliance as Code is the idea that requirements for auditing and compliance teams can be collected with zero
additional cognitive load from developers and other team members participating in software delivery. Because
GitLab has an end-to-end view of the artifacts that are associated with releases (issues, merge requests,
feature flags, and so on) we are uniquely positioned to provide a comprehensive view of compliance-related
activity, collected on an ongoing basis throughout the release process.

Watch for us to provide features that automatically collect this data as part of normal, automated day-to-day
operation of GitLab - whether through issue updates, CI pipelines, test runs, and more, and then make it all
available in an easy to digest format associated with your releases and milestones.

The primary home for this theme, at least in terms of where you will interact with it in the product,
is our [Release Governance](/direction/release/release_governance/) category.
You can read more about what we have planned next at that stage.

## What's Next for CI/CD

<%= direction %>
