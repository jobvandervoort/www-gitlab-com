---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2019-07-31

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Based in APAC                             | 51    | 6.68%       |
| Based in EMEA                             | 212   | 27.75%      |
| Based in LATAM                            | 15    | 1.96%       |
| Based in NA                               | 486   | 63.61%      |
| Total Team Members                        | 764   | 100%        |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Men                                       | 558   | 73.04%      |
| Women                                     | 206   | 26.96%      |
| Other Gender Identities                   | 0     | 0%          |
| Total Team Members                        | 764   | 100%        |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Men in Leadership                         | 36    | 75.00%      |
| Women in Leadership                       | 12    | 25.00%      |
| Other Gender Identities                   | 0     | 0%          |
| Total Team Members                        | 48    | 100%        |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Men in Development                        | 292   | 83.91%      |
| Women in Development                      | 56    | 16.09%      |
| Other Gender Identities                   | 0     | 0%          |
| Total Team Members                        | 348   | 100%        |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 28    | 6.06%       |
| Black or African American                 | 14    | 3.03%       |
| Hispanic or Latino                        | 28    | 6.06%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.22%       |
| Two or More Races                         | 19    | 4.11%       |
| White                                     | 271   | 58.66%      |
| Unreported                                | 101   | 21.86%      |
| Total Team Members                        | 462   | 100%        |

| Race/Ethnicity in Development (US Only)   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 9     | 6.52%       |
| Black or African American                 | 3     | 2.17%       |
| Hispanic or Latino                        | 9     | 6.52%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 7     | 5.07%       |
| White                                     | 83    | 60.14%      |
| Unreported                                | 27    | 19.57%      |
| Total Team Members                        | 138   | 100%        |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 5     | 12.82%      |
| Black or African American                 | 0     | 0.00%       |
| Hispanic or Latino                        | 0     | 0.00%       |
| Native Hawaiian or Other Pacific Islander | 1     | 2.56%       |
| Two or More Races                         | 1     | 2.56%       |
| White                                     | 22    | 56.41%      |
| Unreported                                | 10    | 25.64%      |
| Total Team Members                        | 39    | 100%        |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 56    | 7.33%       |
| Black or African American                 | 20    | 2.62%       |
| Hispanic or Latino                        | 40    | 5.24%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.13%       |
| Two or More Races                         | 25    | 3.27%       |
| White                                     | 422   | 55.24%      |
| Unreported                                | 200   | 26.18%      |
| Total Team Members                        | 764   | 100%        |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 29    | 8.45%       |
| Black or African American                 | 6     | 1.75%       |
| Hispanic or Latino                        | 20    | 5.83%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 11    | 3.21%       |
| White                                     | 189   | 55.10%      |
| Unreported                                | 88    | 25.66%      |
| Total Team Members                        | 343   | 100%        |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 5     | 10.42%      |
| Black or African American                 | 0     | 0.00%       |
| Hispanic or Latino                        | 1     | 2.08%       |
| Native Hawaiian or Other Pacific Islander | 1     | 2.08%       |
| Two or More Races                         | 1     | 2.08%       |
| White                                     | 25    | 52.08%      |
| Unreported                                | 15    | 31.25%      |
| Total Team Members                        | 48    | 100%        |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| 18-24                                     | 16    | 2.09%       |
| 25-29                                     | 146   | 19.11%      |
| 30-34                                     | 219   | 28.66%      |
| 35-39                                     | 151   | 19.76%      |
| 40-49                                     | 159   | 20.81%      |
| 50-59                                     | 64    | 8.38%       |
| 60+                                       | 7     | 0.92%       |
| Unreported                                | 2     | 0.26%       |
| Total Team Members                        | 764   | 100%        |


Source: GitLab's HRIS, BambooHR
